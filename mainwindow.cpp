#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QRegularExpressionValidator>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMessageBox>
#include <QMap>
#include <QFile>
#include <QDebug>

#include "codebase/utility/httpclient.h"
#include "codebase/utility/utils.h"
#include "codebase/ec/eos_key_encode.h"
#include "codebase/chain/chainmanager.h"
#include "codebase/chain/packedtransaction.h"
#include "codebase/Crypto/sha2.h"

#include "secp256k1/include/secp256k1.h"
#include "secp256k1/include/secp256k1_commitment.h"
#include "secp256k1/include/secp256k1_generator.h"
#include "secp256k1/include/secp256k1_elgamal.h"
#include "secp256k1/include/secp256k1_bulletproofs.h"


extern QString base_url;
extern QString url_port;

static QString account;
static QString private_key;

static QString contract = "ciphercipher";
static QString transferp2p = "transferpp";
static QString transferp2c = "transferpc";
static QString transferc2c = "transfercc";
static QString transferc2p = "transfercp";

static const size_t MAX_PROOF_SIZE = 700;

enum type {
    p2p,
    p2c,
    c2c,
    c2p,
    t_max,
};

static QMap<int, QString> transferMap = {
    {p2p, transferp2p},
    {p2c, transferp2c},
    {c2c, transferc2c},
    {c2p, transferc2p}
};

secp256k1_context* _get_context() {
    static secp256k1_context* ctx = secp256k1_context_create(SECP256K1_CONTEXT_SIGN | SECP256K1_CONTEXT_VERIFY);
    return ctx;
}

secp256k1_scratch_space* _get_scratch_space() {
    static secp256k1_scratch_space *scratch = nullptr;
    static int inited = 0;
    if (!inited) {
        scratch = secp256k1_scratch_space_create(_get_context(), 256 * (1 << 20));
    }
    return scratch;
}

secp256k1_bulletproof_generators* _get_bp_generators() {
    static secp256k1_bulletproof_generators *generators = nullptr;
    static int inited = 0;
    if (!inited) {
        generators = secp256k1_bulletproof_generators_create(_get_context(), &secp256k1_generator_const_g, 128);
    }
    return generators;
}

int prove_bulletproof(const secp256k1_context* ctx, unsigned char * proof, size_t *plen, uint64_t value, const unsigned char *blind) {
    return secp256k1_bulletproof_rangeproof_prove(ctx, _get_scratch_space(), _get_bp_generators(), proof, plen, nullptr, nullptr, nullptr, &value, nullptr, &blind, nullptr, 1, &secp256k1_generator_const_h, 64, blind, nullptr, nullptr, 0, nullptr);;
}

int verify_bulletproof(const secp256k1_context* ctx, const secp256k1_pedersen_commitment *commit, const unsigned char * proof, size_t plen) {
    return secp256k1_bulletproof_rangeproof_verify(ctx, _get_scratch_space(), _get_bp_generators(), proof, plen, nullptr, commit, 1, 64, &secp256k1_generator_const_h, nullptr, 0);
}

void roll_random(unsigned char *r, size_t len)
{
    if (len < 32) return;
    for(int i = 0; i < 32; ++i) {
        r[i] = (rand() % 255);
    }
}

char * bin_to_hex(const unsigned char *bin, size_t binsz, char *result, size_t hexsz)
{
    unsigned char hex_str[]= "0123456789abcdef";
    unsigned int i;

    if (hexsz < binsz * 2) return NULL;

    if (!binsz) return NULL;

    for (i = 0; i < binsz; i++) {
        result[i * 2 + 0] = hex_str[(bin[i] >> 4) & 0x0F];
        result[i * 2 + 1] = hex_str[(bin[i]     ) & 0x0F];
    }
    return result;
}

int get_hex(char c)
{
    if(c >= '0' && c<= '9')
        return c - '0';
    else if(c >= 'a' && c<= 'f')
        return c - 'a' + 10;
    else if(c >= 'A' && c<= 'F')
        return c - 'A' + 10;
    else
        return -1;//error
}

unsigned char* hex_to_bin(const char* hexStr, size_t hexsz, unsigned char * result, size_t binsz)
{
    if (binsz * 2 < hexsz) return NULL;

    unsigned long hexLen = strlen(hexStr);
    for(int i = 0; i < hexLen; i +=2)
    {
        result[i/2] = (get_hex(hexStr[i])*16 + get_hex(hexStr[i+1]));
    }

    return result;
}

void MainWindow::_init_lib()
{
    static const secp256k1_context* ctx = _get_context();
    static secp256k1_scratch_space* space = _get_scratch_space();
    static secp256k1_bulletproof_generators* generators = _get_bp_generators();
    (void)ctx;
    (void)space;
    (void)generators;
}

QString MainWindow::_get_public_key(const QString& name)
{
    QString pubkey;
    if (name.isEmpty() || name.size() > 12) {
        return pubkey;
    }

    QJsonObject obj;
    obj.insert("account_name", QJsonValue(name));

    auto data = QJsonDocument(obj).toJson();
    QEventLoop loop;
    HttpClient httpc;

    connect(&httpc, &HttpClient::responseData, [&](const QByteArray& d){
        QJsonArray resArray = QJsonDocument::fromJson(d).object().value("permissions").toArray();
        for (int i = 0; i < resArray.size(); ++i) {
            auto pobj = resArray.at(i).toObject();
            if (pobj.value("perm_name").toString() == "active") {
                pubkey = pobj.value("required_auth").toObject().value("keys").toArray().at(0).toObject().value("key").toString();
                break;
            }
        }

        loop.quit();
    });
    httpc.request(FunctionID::get_account, data);
    loop.exec();

    return pubkey;
}

void MainWindow::init_control_data()
{
    ui->lineEditIP->setText("http://8.217.26.65");
    ui->lineEditPort->setText("8888");
    ui->lineEditContract->setText("token");
    ui->lineEditAccount->setText("alice");
    ui->lineEditKey->setText("5JFFNssyBuejfeTNgxPSUa8GAJMSjAhrX2CdBRu12w7DAK6fLKP");

    base_url = ui->lineEditIP->text();
    url_port = ui->lineEditPort->text();
    contract = ui->lineEditContract->text();
    account = ui->lineEditAccount->text();
    private_key = ui->lineEditKey->text();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->lineEditPort->setValidator(new QRegularExpressionValidator(QRegularExpression("(\\d{1,5})"), this));

    _init_lib();

    init_control_data();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonConnect_clicked()
{
    base_url = ui->lineEditIP->text();
    url_port = ui->lineEditPort->text();

    HttpClient httpc;
    QEventLoop loop;

    connect(&httpc, &HttpClient::responseData, [&](const QByteArray& ba){
        QJsonDocument doc = QJsonDocument::fromJson(ba);
        QJsonObject resObj = doc.object();
        if (resObj.contains("chain_id")) {
            QMessageBox::information(nullptr, "Tip", "Set network succeed!");
        } else {
            QMessageBox::warning(nullptr, "Error", "Connect failed, try again!");
        }
        loop.quit();
    });
    httpc.request(FunctionID::get_info);
    loop.exec();
}

void MainWindow::on_pushButtonRefresh_clicked()
{
    ui->lineEditPlainBalance->clear();
    ui->textEditCipherBalance->clear();
    ui->lineEditRevealedBalance->clear();

    QJsonObject obj;
    obj.insert("code", QJsonValue(contract));
    obj.insert("table", QJsonValue("accounts"));
    obj.insert("scope", QJsonValue(account));
    obj.insert("json", QJsonValue(true));
    obj.insert("limit", QJsonValue(1));

    auto data = QJsonDocument(obj).toJson();
    QEventLoop loop;
    HttpClient httpc;

    connect(&httpc, &HttpClient::responseData, [&](const QByteArray& d){
        QJsonArray resArray = QJsonDocument::fromJson(d).object().value("rows").toArray();
        if (!resArray.isEmpty()) {
            auto a = resArray.at(0).toObject();
            ui->lineEditPlainBalance->setText(a.value("balance").toString());
            ui->textEditCipherBalance->setText(a.value("cipher").toString());
        } else {
            QMessageBox::warning(nullptr, "Error", "Query account profile failed!");
        }

        loop.quit();
    });
    httpc.request(FunctionID::get_table, data);
    loop.exec();
}

void MainWindow::on_pushButtonReveal_clicked()
{
    ui->lineEditRevealedBalance->clear();
    auto sk = eos_key::get_private_key_by_wif(private_key.toStdString());
    if (sk.size() != 33) {
        QMessageBox::warning(nullptr, "Error", "Private key invalid!");
        return;
    }

    if (ui->textEditCipherBalance->toPlainText().isEmpty()) {
        //QMessageBox::warning(nullptr, "Error", "Cipher balance invalid!");
        return;
    }

    std::string hexCipher = ui->textEditCipherBalance->toPlainText().toStdString();
    std::vector<unsigned char> vc = std::vector<unsigned char>(hexCipher.begin(), hexCipher.end());
    auto cipherbalance = Utils::convertHexStrToBytes(vc);
    unsigned char v[64] = { 0 };
    unsigned int val = 0;

    if (secp256k1_elgamal_decrypt(_get_context(), v, cipherbalance.data(), sk.data()) &&
            secp256k1_elgamal_find_v(_get_context(), &val, v)) {
        ui->lineEditRevealedBalance->setText(QString::number(val) + QString(" ZOS"));
        QMessageBox::information(nullptr, "Tip", "Decrypt cipher balance succeed!");
    } else {
        QMessageBox::warning(nullptr, "Error", "Decrypt cipher balance failed!");
    }
}

void MainWindow::on_pushButtonSet_clicked()
{
    if (ui->lineEditContract->text().isEmpty() ||
            ui->lineEditAccount->text().isEmpty() ||
            ui->lineEditKey->text().isEmpty()) {
        QMessageBox::warning(nullptr, "Error", "Contract, Account, Private key\n line can't be empty!");
        return;
    }

    contract = ui->lineEditContract->text();
    account = ui->lineEditAccount->text();
    private_key = ui->lineEditKey->text();

    QMessageBox::information(nullptr, "Tip", "Set account succeed!");
}

void MainWindow::on_pushButtonGenerate_clicked()
{
    on_pushButtonRefresh_clicked();
    on_pushButtonReveal_clicked();

    auto index = ui->comboBoxType->currentIndex();
    QString action = transferMap.value(index, QString());
    if (action.isEmpty()) {
        QMessageBox::warning(nullptr, "Error", "Unknown error with transfer type!");
        return;
    }

    auto to = ui->lineEditTo->text();
    if (to.isEmpty()) {
        QMessageBox::warning(nullptr, "Error", "To Account can't be empty!");
        return;
    }

    auto amount = ui->spinBoxAmount->value();
    if (index != 2 && !amount) {
        QMessageBox::warning(nullptr, "Error", "Amount must be positive!");
        return;
    }

    QJsonObject objArgs = packArgs(to, amount, ui->textEditMemo->toPlainText(), index);
    ui->textEditPreview->setText(QString(QJsonDocument(objArgs).toJson()));
}


void MainWindow::on_pushButtonSend_clicked()
{
    auto index = ui->comboBoxType->currentIndex();
    QString action = transferMap.value(index, QString());
    if (action.isEmpty()) {
        QMessageBox::warning(nullptr, "Error", "Unknown error with transfer type!");
        return;
    }

    QJsonObject obj = QJsonDocument::fromJson(ui->textEditPreview->toPlainText().toLocal8Bit()).object();
    QString bin = abi_json_to_bin(obj, index);
    if (bin.isEmpty()) {
        QMessageBox::warning(nullptr, "Error", "abi json to bin failed!");
        return;
    }

    QByteArray info = get_info();
    if (info.isEmpty()) {
        QMessageBox::warning(nullptr, "Error", "network down!");
        return;
    }

    QJsonArray keys = get_required_keys(bin, info, contract, index);
    if (keys.isEmpty() || !keys.size()) {
        QMessageBox::warning(nullptr, "Error", "private key invalid");
        return;
    }

    QString trx_id = push_transaction(bin, info, contract, index);
    if (!trx_id.isEmpty()) {
        QMessageBox::information(nullptr, "Tip", "Send Balance succeed!");
        on_pushButtonRefresh_clicked();
        on_pushButtonReveal_clicked();
    } else {
        QMessageBox::warning(nullptr, "Error", "Send balance failed, check error.log file!");
    }
}

void MainWindow::_generate_equal_prove(char* hexout, size_t len, const unsigned char *r1, size_t rlen1, uint origin)
{
    if (len < 576 || rlen1 < 32) return;

    QString pubkey = _get_public_key(account);
    if (pubkey.isEmpty()) return;
    auto pk = eos_key::get_public_key_char(pubkey.toStdString());

    secp256k1_pubkey pub;
    auto res = secp256k1_ec_pubkey_parse(_get_context(), &pub, pk.data(), 33);
    if (!res) return;

    unsigned char balance_appro[128] = { 0 }; // ca1
    unsigned char eq_prove[160] = { 0 };

    auto strCipher = ui->textEditCipherBalance->toPlainText().toStdString();
    std::vector<unsigned char> ct = Utils::convertHexStrToBytes(std::vector<unsigned char>(strCipher.begin(), strCipher.end()));

    if (!secp256k1_elgamal_encrypt(_get_context(), balance_appro, &pub, r1, origin)) return;
    if (!secp256k1_elgamal_equal_prove(_get_context(), eq_prove, ct.data(), balance_appro, eos_key::get_private_key_by_wif(private_key.toStdString()).data(), r1)) return;

    bin_to_hex(balance_appro, 128, hexout, 256);
    bin_to_hex(eq_prove, 160, &hexout[256], 320);
}

void MainWindow::_generate_from_valid_prove(char* hexout, size_t len, const unsigned char *r2, size_t rlen2, uint left_a)
{
    if (len < 1990 || rlen2 < 32) return;

    QString pubkey = _get_public_key(account);
    if (pubkey.isEmpty()) return;
    auto pk = eos_key::get_public_key_char(pubkey.toStdString());

    secp256k1_pubkey pub;
    auto res = secp256k1_ec_pubkey_parse(_get_context(), &pub, pk.data(), 33);
    if (!res) return;

    unsigned char balance_left_a[128] = { 0 }; // ca2
    unsigned char from_valid_prove[192] = { 0 };

    if (!secp256k1_elgamal_encrypt(_get_context(), balance_left_a, &pub, r2, left_a)) return;

    unsigned char proof[MAX_PROOF_SIZE] = { 0 };
    size_t plen = MAX_PROOF_SIZE;
    if (!prove_bulletproof(_get_context(), proof, &plen, left_a, r2)) return;

    if (!secp256k1_elgamal_valid_prove(_get_context(), from_valid_prove, &pub, r2, r2, r2, left_a)) return;

    bin_to_hex(balance_left_a, 128, hexout, 256);
    bin_to_hex(proof, 675, &hexout[256], 1350);
    bin_to_hex(from_valid_prove, 192, &hexout[256 + 1350], 384);
}

void MainWindow::_generate_to_valid_prove(char* hexout, size_t len, const unsigned char *r3, size_t rlen3, const QString& name, uint quantity)
{
    if (len < 1990 || rlen3 < 32) return;

    QString pubkey = _get_public_key(name);
    if (pubkey.isEmpty()) return;
    auto pk = eos_key::get_public_key_char(pubkey.toStdString());

    secp256k1_pubkey pub;
    auto res = secp256k1_ec_pubkey_parse(_get_context(), &pub, pk.data(), 33);
    if (!res) return;

    unsigned char balance_delta[128] = { 0 }; // cb1
    unsigned char to_valid_prove[192] = { 0 };

    if (!secp256k1_elgamal_encrypt(_get_context(), balance_delta, &pub, r3, quantity)) return;

    unsigned char proof[MAX_PROOF_SIZE] = { 0 };
    size_t plen = MAX_PROOF_SIZE;
    if (!prove_bulletproof(_get_context(), proof, &plen, quantity, r3)) return;

    if (!secp256k1_elgamal_valid_prove(_get_context(), to_valid_prove, &pub, r3, r3, r3, quantity)) return;

    bin_to_hex(balance_delta, 128, hexout, 256);
    bin_to_hex(proof, 675, &hexout[256], 1350);
    bin_to_hex(to_valid_prove, 192, &hexout[256 + 1350], 384);
}

void MainWindow::_generate_ecdsa_signature(char* hexsig, size_t len,
                                      const char* eqp, size_t len1,
                                      const char* fvp, size_t len2,
                                      const char* tvp, size_t len3,
                                      const unsigned char *r1, size_t rlen1,
                                      const unsigned char *r2, size_t rlen2,
                                      const unsigned char *r3, size_t rlen3)
{
    if (len < 128 || len1 < 576 || len2 < 1990 || len3 < 1990) return;

    unsigned char eq_para_sz[288] = { 0 };
    unsigned char from_pub_para_sz[995] = { 0 };
    unsigned char to_pub_para_sz[995] = { 0 };

    hex_to_bin(eqp, 576, eq_para_sz, 288);
    hex_to_bin(fvp, 1990, from_pub_para_sz, 995);
    hex_to_bin(tvp, 1990, to_pub_para_sz, 995);

    unsigned char hash[32] = { 0 };
    SHA256_CTX hasher;
    sha256_Init(&hasher);
    sha256_Update(&hasher, eq_para_sz, 288);
    sha256_Update(&hasher, from_pub_para_sz, 995);
    sha256_Update(&hasher, to_pub_para_sz, 995);
    sha256_Final(&hasher, hash);

    unsigned char seckey[32] = { 0 };
    if (!secp256k1_elgamal_seckey(_get_context(), seckey, r1, r2,  r3)) return;

    secp256k1_ecdsa_signature signature;
    memset(&signature.data, 0, sizeof(signature.data));
    if (!secp256k1_ecdsa_sign(_get_context(), &signature, hash, seckey, nullptr, nullptr)) return;

    bin_to_hex(signature.data, 64, hexsig, 128);
}

bool MainWindow::packpc(QJsonObject& args, const QString& name, uint quantity)
{
    std::vector<unsigned char> r(32);
    roll_random(r.data(), 32);
    QString pubkey = _get_public_key(name);
    if (pubkey.isEmpty()) return false;
    auto pk = eos_key::get_public_key_char(pubkey.toStdString());
    std::vector<unsigned char> ciphertext(128);
    secp256k1_pubkey pub;
    auto res = secp256k1_ec_pubkey_parse(_get_context(), &pub, pk.data(), 33);
    if (!res) return false;
    res = secp256k1_elgamal_encrypt(_get_context(), ciphertext.data(), &pub, r.data(), quantity);
    if (!res) return false;

    std::vector<unsigned char> rand = Utils::convertBytesToHexStr(r);
    std::vector<unsigned char> ct = Utils::convertBytesToHexStr(ciphertext);
    args.insert("random", QJsonValue(QString::fromStdString(std::string(rand.begin(), rand.end()))));
    args.insert("ciphertext", QJsonValue(QString::fromStdString(std::string(ct.begin(), ct.end()))));

    return true;
}

bool MainWindow::packcc(QJsonObject& args, const QString& name, uint quantity)
{
    unsigned char r1[32] = { 0 };
    unsigned char r2[32] = { 0 };
    unsigned char r3[32] = { 0 };

    char hex_eqp[576 + 1] = { 0 };
    char hex_fvp[1990 + 1] = { 0 };
    char hex_tvp[1990 + 1] = { 0 };
    char hex_sig[128 + 1] = { 0 };

    roll_random(r1, 32);
    roll_random(r2, 32);
    roll_random(r3, 32);

    bool ok = false;
    uint cipherBalance = ui->lineEditRevealedBalance->text().split(' ').at(0).toUInt(&ok);
    if (!ok) return false;

    if (quantity > cipherBalance) return false;

    _generate_equal_prove(hex_eqp, 577, r1, 32, cipherBalance);
    _generate_from_valid_prove(hex_fvp, 1990, r2, 32, cipherBalance - quantity);
    _generate_to_valid_prove(hex_tvp, 1990, r3, 32, name, quantity);

    _generate_ecdsa_signature(hex_sig, 128, hex_eqp, 576, hex_fvp, 1990, hex_tvp, 1990, r1, 32, r2, 32, r3, 32);

    args.insert("eq_para", QJsonValue(QString::fromStdString(std::string(hex_eqp))));
    args.insert("from_pub_para", QJsonValue(QString::fromStdString(std::string(hex_fvp))));
    args.insert("to_pub_para", QJsonValue(QString::fromStdString(std::string(hex_tvp))));
    args.insert("signature", QJsonValue(QString::fromStdString(std::string(hex_sig))));
    return true;
}

bool MainWindow::packcp(QJsonObject& args, const QString& name, uint quantity)
{
    unsigned char r1[32] = { 0 };
    unsigned char r2[32] = { 0 };
    unsigned char r3[32] = { 0 };

    char hex_r3[65] = { 0 };

    char hex_eqp[576 + 1] = { 0 };
    char hex_fvp[1990 + 1] = { 0 };
    char hex_tvp[1990 + 1] = { 0 };
    char hex_sig[128 + 1] = { 0 };

    roll_random(r1, 32);
    roll_random(r2, 32);
    roll_random(r3, 32);

    bin_to_hex(r3, 32, hex_r3, 64);

    bool ok = false;
    uint cipherBalance = ui->lineEditRevealedBalance->text().split(' ').at(0).toUInt(&ok);
    if (!ok) return false;

    if (quantity > cipherBalance) return false;

    _generate_equal_prove(hex_eqp, 577, r1, 32, cipherBalance);
    _generate_from_valid_prove(hex_fvp, 1990, r2, 32, cipherBalance - quantity);
    _generate_to_valid_prove(hex_tvp, 1990, r3, 32, name, quantity);

    _generate_ecdsa_signature(hex_sig, 128, hex_eqp, 576, hex_fvp, 1990, hex_tvp, 1990, r1, 32, r2, 32, r3, 32);

    args.insert("eq_para", QJsonValue(QString::fromStdString(std::string(hex_eqp))));
    args.insert("from_pub_para", QJsonValue(QString::fromStdString(std::string(hex_fvp))));
    args.insert("to_pub_para", QJsonValue(QString::fromStdString(std::string(hex_tvp))));
    args.insert("signature", QJsonValue(QString::fromStdString(std::string(hex_sig))));
    args.insert("random", QJsonValue(QString::fromStdString(std::string(hex_r3))));
    return true;
}

QJsonObject MainWindow::packArgs(const QString& name, uint quantity, const QString& memo, int action)
{
    QJsonObject args;
    if (action < p2p || action >= t_max) {
        return args;
    }

    args.insert("from", QJsonValue(account));
    args.insert("to", QJsonValue(name));
    args.insert("memo", QJsonValue(memo));

    switch(action) {
        case 0:
        {
            args.insert("quantity", QJsonValue(QString::number(quantity) + QString(" ZOS")));
            break;
        }
        case 1:
        {
            if (!packpc(args, name, quantity)) break;
            args.insert("quantity", QJsonValue(QString::number(quantity) + QString(" ZOS")));
            args.insert("symbol", QJsonValue("0,ZOS"));
            break;
        }
        case 2:
        {
            if (!packcc(args, name, quantity)) break;
            args.insert("symbol", QJsonValue("0,ZOS"));
            break;
        }
        case 3:
        {
            if (!packcp(args, name, quantity)) break;
            args.insert("quantity", QJsonValue(QString::number(quantity) + QString(" ZOS")));
            args.insert("symbol", QJsonValue("0,ZOS"));
            break;
        }
    }

    return args;
}

QString MainWindow::abi_json_to_bin(const QJsonObject& objArgs, int action)
{
    QString binargs;
    HttpClient httpc;
    QEventLoop loop;

    QJsonObject obj;
    obj.insert("code", QJsonValue(contract));
    obj.insert("action", QJsonValue(transferMap.value(action)));
    obj.insert("args", objArgs);

    connect(&httpc, &HttpClient::responseData, [&](const QByteArray& ba){
        QJsonDocument doc = QJsonDocument::fromJson(ba);
        QJsonObject resObj = doc.object();
        QString result;
        if (!resObj.isEmpty() && resObj.contains("binargs")) {
            binargs = resObj.value("binargs").toString();
        }
        loop.quit();
    });
    httpc.request(FunctionID::abi_json_to_bin, QJsonDocument(obj).toJson());
    loop.exec();

    return binargs;
}

QByteArray MainWindow::get_info()
{
    QByteArray info;
    HttpClient httpc;
    QEventLoop loop;

    connect(&httpc, &HttpClient::responseData, [&](const QByteArray& ba){
        QJsonDocument doc = QJsonDocument::fromJson(ba);
        QJsonObject resObj = doc.object();
        if (resObj.contains("chain_id")) {
            info = ba;
        }
        loop.quit();
    });
    httpc.request(FunctionID::get_info);
    loop.exec();

    return info;
}

QJsonArray MainWindow::get_required_keys(const QString &binargs, const QByteArray& info, const QString &code, int action)
{
    QJsonArray rkeys;
    HttpClient httpc;
    QEventLoop loop;

    auto active  = ChainManager::getActivePermission(account.toStdString());
    SignedTransaction signedTxn;
    ChainManager::setTransactionHeaderInfo(signedTxn, info);
    ChainManager::addAction(signedTxn, code.toStdString(), transferMap.value(action).toStdString(), binargs.toStdString(), active);

    QJsonArray avaibleKeys;
    avaibleKeys.append(QJsonValue(QString::fromStdString(eos_key::get_eos_public_key_by_wif(private_key.toStdString()))));

    QJsonObject obj;
    obj.insert("available_keys", avaibleKeys);
    obj.insert("transaction", signedTxn.toJson().toObject());

    connect(&httpc, &HttpClient::responseData, [&](const QByteArray& ba){
        QJsonDocument doc = QJsonDocument::fromJson(ba);
        QJsonObject resObj = doc.object();
        if (resObj.contains("required_keys")) {
            rkeys = resObj.value("required_keys").toArray();
        }
        loop.quit();
    });
    httpc.request(FunctionID::get_required_keys, QJsonDocument(obj).toJson());
    loop.exec();

    return rkeys;
}

QString MainWindow::push_transaction(const QString &binargs, const QByteArray &info, const QString &code, int action)
{
    QString trx_id;
    HttpClient httpc;
    QEventLoop loop;

    std::vector<unsigned char> pri = eos_key::get_private_key_by_wif(private_key.toStdString());
    if (pri.empty()) {
        return trx_id;
    }

    QJsonObject inf = QJsonDocument::fromJson(info).object();
    if (inf.isEmpty()) {
        return trx_id;
    }

    auto active  = ChainManager::getActivePermission(account.toStdString());
    SignedTransaction signedTxn;
    ChainManager::setTransactionHeaderInfo(signedTxn, info);
    ChainManager::addAction(signedTxn, code.toStdString(), transferMap.value(action).toStdString(), binargs.toStdString(), active);

    signedTxn.sign(pri, TypeChainId::fromHex(inf.value("chain_id").toString().toStdString()));
    PackedTransaction packedTrx(signedTxn, "none");

    connect(&httpc, &HttpClient::responseData, [&](const QByteArray& ba){
        QJsonDocument doc = QJsonDocument::fromJson(ba);
        QJsonObject resObj = doc.object();
        if (resObj.contains("transaction_id")) {
            trx_id = resObj.value("transaction_id").toString();
        } else {
            QFile file("error.log");
            if (file.open(QIODevice::Append | QIODevice::Text)) {
                file.write(ba);
            }
        }
        loop.quit();
    });
    httpc.request(FunctionID::push_transaction, QJsonDocument(packedTrx.toJson().toObject()).toJson());
    loop.exec();
    return trx_id;
}
