#-------------------------------------------------
#
# Project created by QtCreator 2019-09-16T10:10:49
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = zeos-wallet
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        codebase/Crypto/aes.c \
        codebase/Crypto/base58.c \
        codebase/Crypto/rmd160.c \
        codebase/Crypto/sha2.c \
        codebase/Crypto/sha3.c \
        codebase/Crypto/uECC.c \
        codebase/chain/action.cpp \
        codebase/chain/chainbase.cpp \
        codebase/chain/chainmanager.cpp \
        codebase/chain/eosbytewriter.cpp \
        codebase/chain/eosnewaccount.cpp \
        codebase/chain/packedtransaction.cpp \
        codebase/chain/signedtransaction.cpp \
        codebase/chain/transaction.cpp \
        codebase/chain/transactionextension.cpp \
        codebase/chain/transactionheader.cpp \
        codebase/chain/typeaccountpermissionweight.cpp \
        codebase/chain/typeauthority.cpp \
        codebase/chain/typekeypermissionweight.cpp \
        codebase/chain/typename.cpp \
        codebase/chain/typepermissionlevel.cpp \
        codebase/chain/typewaitweight.cpp \
        codebase/ec/eos_key_encode.cpp \
        codebase/ec/sha256.cpp \
        codebase/ec/sha512.cpp \
        codebase/ec/typechainid.cpp \
        codebase/utility/httpclient.cpp \
        codebase/utility/utils.cpp \
        main.cpp \
        mainwindow.cpp \
        secp256k1/src/secp256k1.c

HEADERS += \
        codebase/Crypto/aes.h \
        codebase/Crypto/aes.hpp \
        codebase/Crypto/libbase58.h \
        codebase/Crypto/macros.h \
        codebase/Crypto/options.h \
        codebase/Crypto/rmd160.h \
        codebase/Crypto/sha2.h \
        codebase/Crypto/sha3.h \
        codebase/Crypto/uECC.h \
        codebase/chain/action.h \
        codebase/chain/chainbase.h \
        codebase/chain/chainmanager.h \
        codebase/chain/eosbytewriter.h \
        codebase/chain/eosnewaccount.h \
        codebase/chain/packedtransaction.h \
        codebase/chain/signedtransaction.h \
        codebase/chain/transaction.h \
        codebase/chain/transactionextension.h \
        codebase/chain/transactionheader.h \
        codebase/chain/typeaccountpermissionweight.h \
        codebase/chain/typeauthority.h \
        codebase/chain/typekeypermissionweight.h \
        codebase/chain/typename.h \
        codebase/chain/typepermissionlevel.h \
        codebase/chain/typewaitweight.h \
        codebase/ec/eos_key_encode.h \
        codebase/ec/sha256.h \
        codebase/ec/sha512.h \
        codebase/ec/typechainid.h \
        codebase/utility/httpclient.h \
        codebase/utility/utils.h \
        mainwindow.h \
        secp256k1/include/secp256k1.h \
        secp256k1/include/secp256k1_aggsig.h \
        secp256k1/include/secp256k1_bulletproofs.h \
        secp256k1/include/secp256k1_commitment.h \
        secp256k1/include/secp256k1_ecdh.h \
        secp256k1/include/secp256k1_elgamal.h \
        secp256k1/include/secp256k1_generator.h \
        secp256k1/include/secp256k1_rangeproof.h \
        secp256k1/include/secp256k1_recovery.h \
        secp256k1/include/secp256k1_schnorrsig.h \
        secp256k1/include/secp256k1_surjectionproof.h \
        secp256k1/include/secp256k1_whitelist.h \
        secp256k1/src/basic-config.h \
        secp256k1/src/ecdsa.h \
        secp256k1/src/ecdsa_impl.h \
        secp256k1/src/eckey.h \
        secp256k1/src/eckey_impl.h \
        secp256k1/src/ecmult.h \
        secp256k1/src/ecmult_const.h \
        secp256k1/src/ecmult_const_impl.h \
        secp256k1/src/ecmult_gen.h \
        secp256k1/src/ecmult_gen_impl.h \
        secp256k1/src/ecmult_impl.h \
        secp256k1/src/ecmult_static_context.h \
        secp256k1/src/field.h \
        secp256k1/src/field_10x26.h \
        secp256k1/src/field_10x26_impl.h \
        secp256k1/src/field_5x52.h \
        secp256k1/src/field_5x52_asm_impl.h \
        secp256k1/src/field_5x52_impl.h \
        secp256k1/src/field_5x52_int128_impl.h \
        secp256k1/src/field_impl.h \
        secp256k1/src/group.h \
        secp256k1/src/group_impl.h \
        secp256k1/src/hash.h \
        secp256k1/src/hash_impl.h \
        secp256k1/src/libsecp256k1-config.h \
        secp256k1/src/modules/aggsig/main_impl.h \
        secp256k1/src/modules/bulletproofs/inner_product_impl.h \
        secp256k1/src/modules/bulletproofs/main_impl.h \
        secp256k1/src/modules/bulletproofs/rangeproof_impl.h \
        secp256k1/src/modules/bulletproofs/util.h \
        secp256k1/src/modules/commitment/main_impl.h \
        secp256k1/src/modules/commitment/pedersen_impl.h \
        secp256k1/src/modules/ecdh/main_impl.h \
        secp256k1/src/modules/elgamal/main_impl.h \
        secp256k1/src/modules/generator/main_impl.h \
        secp256k1/src/modules/rangeproof/borromean.h \
        secp256k1/src/modules/rangeproof/borromean_impl.h \
        secp256k1/src/modules/rangeproof/main_impl.h \
        secp256k1/src/modules/rangeproof/rangeproof.h \
        secp256k1/src/modules/rangeproof/rangeproof_impl.h \
        secp256k1/src/modules/recovery/main_impl.h \
        secp256k1/src/modules/schnorrsig/main_impl.h \
        secp256k1/src/modules/surjection/main_impl.h \
        secp256k1/src/modules/surjection/surjection.h \
        secp256k1/src/modules/surjection/surjection_impl.h \
        secp256k1/src/modules/whitelist/main_impl.h \
        secp256k1/src/modules/whitelist/whitelist_impl.h \
        secp256k1/src/num.h \
        secp256k1/src/num_gmp.h \
        secp256k1/src/num_gmp_impl.h \
        secp256k1/src/num_impl.h \
        secp256k1/src/scalar.h \
        secp256k1/src/scalar_4x64.h \
        secp256k1/src/scalar_4x64_impl.h \
        secp256k1/src/scalar_8x32.h \
        secp256k1/src/scalar_8x32_impl.h \
        secp256k1/src/scalar_impl.h \
        secp256k1/src/scalar_low.h \
        secp256k1/src/scalar_low_impl.h \
        secp256k1/src/scratch.h \
        secp256k1/src/scratch_impl.h \
        secp256k1/src/util.h

FORMS += \
        mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    codebase/LICENSE \
    codebase/README.md

INCLUDEPATH += $$PWD/secp256k1
INCLUDEPATH += $$PWD/secp256k1/src
