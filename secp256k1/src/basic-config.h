/**********************************************************************
 * Copyright (c) 2013, 2014 Pieter Wuille                             *
 * Distributed under the MIT software license, see the accompanying   *
 * file COPYING or http://www.opensource.org/licenses/mit-license.php.*
 **********************************************************************/

#ifndef SECP256K1_BASIC_CONFIG_H
#define SECP256K1_BASIC_CONFIG_H

//optimizations that any compiler we target have
#define HAVE_BUILTIN_CLZLL 1
#define HAVE_BUILTIN_EXPECT 1

#ifndef _WIN32
#define HAVE___INT128 1
#endif

#define HAVE_CONFIG_H 1

//enable asm
#ifdef __x86_64__
  #define USE_ASM_X86_64 1
#endif

#define ENABLE_MODULE_RECOVERY
#define ENABLE_MODULE_GENERATOR
#define ENABLE_MODULE_BULLETPROOF
#define ENABLE_MODULE_ELGAMAL

#define USE_ECMULT_STATIC_PRECOMPUTATION 1

#endif /* SECP256K1_BASIC_CONFIG_H */
