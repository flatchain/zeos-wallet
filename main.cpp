#include "mainwindow.h"
#include <QApplication>
#include <time.h>

QString base_url = "http://127.0.0.1";
QString url_port = "8888";

int main(int argc, char *argv[])
{
    srand((unsigned)time(0));

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
