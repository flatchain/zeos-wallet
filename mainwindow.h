#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void        _init_lib();
    QString     _get_public_key(const QString& name);

    void        _generate_equal_prove(char* hexout, size_t len, const unsigned char *r1, size_t rlen1, uint origin);
    void        _generate_from_valid_prove(char* hexout, size_t len, const unsigned char *r2, size_t rlen2, uint left);
    void        _generate_to_valid_prove(char* hexout, size_t len, const unsigned char *r3, size_t rlen3, const QString& name, uint quantity);
    void        _generate_ecdsa_signature(char* hexsig, size_t len,
                                          const char* eqp, size_t len1,
                                          const char* fvp, size_t len2,
                                          const char* tvp, size_t len3,
                                          const unsigned char *r1, size_t rlen1,
                                          const unsigned char *r2, size_t rlen2,
                                          const unsigned char *r3, size_t rlen3);

    bool packpc(QJsonObject& args, const QString& name, uint quantity);
    bool packcc(QJsonObject& args, const QString& name, uint quantity);
    bool packcp(QJsonObject& args, const QString& name, uint quantity);

    QJsonObject packArgs(const QString& name, uint quantity, const QString& memo, int action);
    QString     abi_json_to_bin(const QJsonObject& objArgs, int action);
    QByteArray  get_info();
    QJsonArray  get_required_keys(const QString& binargs, const QByteArray& info, const QString& code, int action);
    QString     push_transaction(const QString& binargs, const QByteArray& info, const QString& code, int action);

    void    init_control_data();

private slots:
    void on_pushButtonConnect_clicked();

    void on_pushButtonRefresh_clicked();

    void on_pushButtonReveal_clicked();

    void on_pushButtonSet_clicked();

    void on_pushButtonSend_clicked();

    void on_pushButtonGenerate_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
